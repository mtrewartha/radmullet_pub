<?php
include_once('includes/php/db.inc.php');
include_once('includes/php/config.php');
include_once('includes/php/functions.php');

// Authentication System stolen from php.net
$realm = 'Restricted Area';

if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
header('HTTP/1.1 401 Unauthorized');
header('WWW-Authenticate: Digest realm="'.$realm.
        '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

die('You chose, poorly');
}

// analyze the PHP_AUTH_DIGEST variable
if (!($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
!isset($users[$data['username']]))
die('Wrong Credentials!');


// generate the valid response
$A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

if ($data['response'] != $valid_response)
die('Wrong Credentials!');

// ok, valid username & password
//echo 'Welcome: ' . $data['username'];

$yearNow = date("Y");


# Get default quality
$sql = "SELECT defaultQuality from userData";
$result = mysqli_query($con, $sql);
while ($row = mysqli_fetch_assoc($result)) {
    $movieQuality = $row['defaultQuality'];
}

?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Movies</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="includes/js/pace.js"></script>
        <link href="includes/css/pace.css" rel="stylesheet" />
        <link href="includes/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

        <script>
            $(document).ready(function() {
                // When searching for content, do a omdbapi look up for movies, and create a new table with the results.
                // The user can then add one of the returned results to the database.
                $("#searchForm").submit(function() {
                    var searchTitle = $("#searchTitle").val();
                    $.get("http://www.omdbapi.com/?s="+searchTitle,function(data,status) {
                        // Show/Hide search result table
                        $("#searchResults").show();

                        // Alert and return if no film found!
                        if(/Movie not found/.test(data)) {
                            alert("Film not found: "+title);
                            return;
                        }

                        // Convert the JSON string into an object
                        var movies = jQuery.parseJSON(data);
                        // Empty variable we're going to use to populate the search result table
                        var returnTableBody = "";
                        // Iterate over the JSON object, looking for movies only
                        $.each(movies.Search, function(i, item) {
                            // Empty variable for the row of the table we're going to return
                            var returnTableRow = "";
                            // Only want movies
                            if (this.Type === "movie") {
                                var imdbID = this.imdbID;
                                var title = this.Title;
                                var year = this.Year;
                                // Format data nicely for the table this funcion will update.
                                returnTableRow += '<td><a class="movieTitleModal'+i+'" title="click for more info" data-id="'+imdbID+'" id="modal'+i+'" data-toggle="modal" href="#modal'+i+'">'+title+'</a></td>';
                                returnTableRow += '<td><span id="movieYearModal'+i+'">'+year+'</td>';
                                returnTableRow += '<td><a href="http://www.imdb.com/title/'+imdbID+'" target="_blank">IMDB</a></td>';
                                //returnTableRow += '<td><span title="Click to add"><a href="addMovie.php?title='+title+'&year='+year+'&quality='+<?php echo"$movieQuality";?>+'">Add</a></span></td>';
                                returnTableRow += '<td><span title="Click to add"><a href="addMovie.php?imdbID='+imdbID+'">Add</a></span></td>';
                                returnTableBody += '<tr>'+returnTableRow+'</tr>';
                            }
                        });
                        // Update the table
                        $("#searchTableBody").html(returnTableBody);
                    });
                    event.preventDefault();
                });

                // Update the modal title with the movie title and other data
                <?php
                $i = 0;
                while ($i <= 10) {
                    echo "
                    $('#modal".$i."').on('show', function(){
                        var title = $('.movieTitleModal".$i."').text();
                        var year = $('#movieYearModal".$i."').text();
                        var imdbID = $('.movieTitleModal".$i."').attr('data-id');
                        var newTitle = title+' ('+year+')';

                        $.get('http://www.omdbapi.com/?i='+imdbID, function(data,status) {
                            // Convert the JSON string into an object
                            var movie = jQuery.parseJSON(data);
                            var plot = '';
                            var poster = '';
                            // Empty variable we're going to use to populate the search result table
                            // Iterate over the JSON object
                            $.each(movie, function (i,item) {
                                if (i === 'Plot') {
                                    plot = item;
                                }
                                if (i === 'Poster') {
                                    poster = item;
                                }

                            });
                            // update the modal with new details
                            $('#modalTitle".$i."').text(newTitle);
                            $('#modalPicture".$i."').attr('src','includes/php/image.php?image_src='+poster);
                            $('#modalPlot".$i."').text(plot);
                        });
                    });
                    ";
                    $i++;
                }
                ?>

                // dont care about this stage
                //$('#myModal').on('shown', function(){
                    //alert('The modal is shown');
                //});
            });
        </script>

    </head>

    <body>
    <?php
    $i = 0;
    while ($i <= 10) {
        echo '<!-- Movie Details Modal -->
        <div class="modal fade" id="modal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="modalTitle'.$i.'"></h4>
                    </div>
                    <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-7 col-md-7">
                            <div class="thumbnail">
                                <img id="modalPicture'.$i.'" src="includes/img/loader.gif" alt="Movie Poster">
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5">
                            <span id="modalPlot'.$i.'"></span>
                        </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-small" data-dismiss="modal"><i class="icon-white icon-remove-circle"></i> Close</button>
                        <!--<button type="button" class="btn btn-primary btn-small"><i class="icon-white icon-download-alt"></i> Add</button>-->
                    </div>
                </div>
            </div>
        </div>
        ';
        $i++;
    }
    ?>

    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="/movies" class="navbar-brand">Rad Mullet</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li>
              <a href="about.php">About</a>
            </li>
            <li>
              <a href="donate.php">Donate</a>
            </li>
            <li>
              <a href="logs.php">Logs</a>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Suggesting Viewing <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <?php
                    $i = 1;
                    while ($i <= 3) {
                        echo '<li><a tabindex="-1" href="http://www.imdb.com/year/'.$yearNow.'/" target="_blank">Popular Feature Films of '.$yearNow.'</a></li>';
                        $yearNow--;
                        $i++;
                    }
                ?>
                <li><a tabindex="-1" href="http://www.imdb.com/chart/top" target="_blank">Top 250</a></li>
                <li><a tabindex="-1" href="http://www.imdb.com/chart/bottom" target="_blank">Bottom 100</a></li>
              </ul>
            </li>

          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Settings <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <li><a tabindex="-1" href="forceCheck.php"><span class="glyphicon glyphicon-repeat"></span> Force NZB Search</a></li>
                <li><a tabindex="-1" href="deleteEntry.php?movieID=all"><span class="glyphicon glyphicon-remove-circle"></span> Delete Downloaded NZBs</a></li>
                <li><a tabindex="-1" href="../default/"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                <li class="divider"></li>
                <li><a tabindex="-1" href="../cerulean/">User Settings</a></li>
                <li><a tabindex="-1" href="../cosmo/">Search Providers</a></li>
                <li><a tabindex="-1" href="../flatly/">Notifications</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">

                <div class="col-lg-1">
                </div>

                <div class="col-lg-3">
                    <div class="bsa well">
                        <img src="includes/img/logo.gif" alt="logo" class="img-rounded">
                    </div> <!-- bsa well -->
                </div> <!-- col-lg-3 -->

                <div class="col-lg-6">
                    <h1>Rad Mullet</h1>
                    <p class="lead">Usenet Movie Downloader - set it and forget it, we'll do the hard work for you! If your desired movie hasnt been released yet, we'll search till we find it!</p>
                    <form id="searchForm" class="form-inline" role="form">
                        <div class="form-group">
                            <label class="sr-only" for="movieSearch">Title</label>
                            <input type="text" class="form-control" id="searchTitle" placeholder="Enter Movie Name">
                        </div> <!-- form group -->
                        <button id="searchButton" type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Search...</button>
                    </form>
                </div> <!-- col-lg-6 -->

                <div class="col-lg-2">
                </div>

            </div> <!-- row -->
        </div> <!-- page-header -->

        <div class="row" id="searchResults" style="display:none">
                <h2><span class="label label-default">Search Results</span></h2>

            <table class="table table-striped">
                <thead>
                    <th>Title</th>
                    <th>Year</th>
                    <th>External Link</th>
                    <th>&nbsp;</th>
                </thead>
                <tbody id="searchTableBody">
                </tbody>
            </table>
        </div>

        <div class="row">
            <h2><span class="label label-default">Movie List</span></h2>
        </div>

        <div class="row">
            <table class="table table-striped">
                <thead>
                    <th>Title</th>
                    <th>Year</th>
                    <th>Quality</th>
                    <th>Status</th>
                </thead>
                <tbody>

                <?php
                    $sql = "SELECT m.id as id, m.name as name, m.year as year, q.value as quality, s.value as status
                            FROM movieData m
                            LEFT JOIN qualityLookup q
                            ON m.quality = q.id
                            LEFT JOIN statusLookup s
                            ON m.status = s.id
                            ORDER BY s.value DESC, m.dateAdded";
                    $result = mysqli_query($con, $sql);

                    if ( ! $con->query($sql) ) {
                        printf("%s\n", $con->error);
                            die;
                    }

                    while ($row = mysqli_fetch_assoc($result)) {
                        print "<tr><td><a href=\"movieDetails.php?id=".$row['id']."\">".$row['name']."</a></td>";
                        print "<td>".$row['year']."</td>";
                        print "<td>".$row['quality']."</td>";
                        print "<td>";
                        if ($row['status'] == "Downloaded") {
                            print "<span class=\"label label-success\">".$row['status']."</span";
                        }
                        else  {
                            print "<span class=\"label label-primary\">".$row['status']."</span";
                        }
                        
                        print"</td></tr>";
                    }
                ?>
                </tbody>
            </table>
        </div> <!-- row -->

        <div class="row">
            <h5><span class="label label-default">X Movies Dowloaded Since Y</span></h5>    
        </div>

    </div> <!-- container -->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="http://code.jquery.com/jquery.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="includes/js/bootstrap.min.js"></script>

    </body>
</html>

<?php
mysqli_close($con);
?>
