# This is my README

To add users for authentication, edit the file includes/php/config.php.
Set up your DB connection, edit the file includes/php/db.inc.php.

# Feature Request List 
1) Get user auth from DB
2) Using the log, add a feature to redownload an nzb but ignore the last downloaded nzb (by GUID) - ie got a bad release
3) Get all newznab providers working with the API
4) Get all pages uniformly themed with css on index.php (some are running logo on left, should be right, some have no css, etc)
5) Remove dependence from OMDBAPI (cause its only updated monthly, and may not have films that IMDB knows about for a few weeks).
6) Add a menu option to add a movie by IMDBID rather than searching by name
7) Remove hardcoded low quality array, add to DB, add option to have users customise it.
