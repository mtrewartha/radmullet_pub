<?php

// function to parse the http auth header
function http_digest_parse($txt)
{
    // protect against missing data
    $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
    $data = array();
    $keys = implode('|', array_keys($needed_parts));

    preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

    foreach ($matches as $m) {
        $data[$m[1]] = $m[3] ? $m[3] : $m[4];
        unset($needed_parts[$m[1]]);
    }

    return $needed_parts ? false : $data;
}


// function to return an array of the movie details
function getMovieDetails($imdbID) {

    // Create empty array
    $array = array();

    // Get JSON and push the required elements into the array
    $url = "http://www.omdbapi.com/?i=".$imdbID;
    $obj = json_decode(file_get_contents($url));
    array_push ($array, $obj->Title);
    array_push ($array, $obj->Year);
    array_push ($array, $obj->imdbID);
    array_push ($array, $obj->Poster);

    // Return the array
    return $array;
}


// function to return a list of the search providers as associated arrays
function getSearchProviders($con) {
    $sql = "SELECT providerName, providerURL, providerAPI FROM searchProviders WHERE active = 1";
    $result = mysqli_query($con, $sql);
    if ( ! $con->query($sql) ) {
        printf("%s\n", $con->error);
        die;
    }
    
    $returnArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $array = array (
            "providerName" => $row['providerName'],
            "providerURL" => $row['providerURL'],
            "providerAPI" => $row['providerAPI'],
        );
        array_push($returnArray, $array);
    }

    return $returnArray;
}


// function to return a list of the movies to search for
function getMovies($con) {
    $sql = "SELECT m.name as name, m.year as year, m.quality, m.imdbID as imdbID, q.value as quality
            FROM movieData m
            LEFT JOIN qualityLookup q
            ON m.quality = q.id
            WHERE m.status != 1";
    $result = mysqli_query($con, $sql);
    if ( ! $con->query($sql) ) {
        printf("%s\n", $con->error);
        die;
    }

    $returnArray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $array = array (
            "name" => $row['name'],
            "year" => $row['year'],
            "quality" => $row['quality'],
            "imdbID" => $row['imdbID'],
        );
        array_push($returnArray, $array);
    }

    return $returnArray;
}


// function to search the requested search provider for the nzb, and add to the cart
function addFilmToCart($con, $searchProvider, $movie) {

    $lowQuality = array("cam", "camrip", "ts", "hqcam",
                        "telesync", "pdvd",
                        "wp", "workprint",
                        "tc", "telecine",
                        "ppv", "ppvrip",
                        "scr", "screener", "dvdscr", "dvdscreener", "bdscr", "webscr",
                        "ddc",
                        "r5", "r6",
                        "dvdrip", "dvdr", "dvd-full", "full-rip", "dvd-5", "dvd-9", "dvd5", "dvd9",
                        "vodrip", "vodr",
                        "web-rip", "webrip",
                        );

    $reqQuality = $movie['quality'];
    $reqQuality = str_replace("p", "", $reqQuality);
    $reqName = $movie['name'];
    $reqYear = $movie['year'];
    $reqimdbID = $movie['imdbID'];
    $reqimdbID = str_replace("tt", "", $reqimdbID);
    $api = $searchProvider['providerAPI'];
    $url = $searchProvider['providerURL'];
    $searchProviderName = $searchProvider['providerName'];
    $url = $url."api?apikey=".$api."&attrs=grabs&t=movie&imdbid=".$reqimdbID;

    $xmlString = file_get_contents($url);
    if ($xmlString === false) {
        logit($con, "ERR", "Error connecting", NULL, NULL, NULL, $searchProvider['providerName']);
        return false;
    }
    $xml = simplexml_load_string($xmlString);

    $nzbArray = array();

    foreach($xml->channel->item as $item) {
        // Only find movies which quality in the name match what we've requested
        $title = $item->title;

        // Checks for HD stuff
        if (($reqQuality == 1080) || ($reqQuality == 720)) {
            // Ensure the title contains the quality we're after
            if (strpos($title, $reqQuality) === FALSE) {
                continue;
            }
            // If the title contains a low quality version, skip it
            if ($title != preg_replace("/\W(".join('|', $lowQuality).")\W/i", "", $title)) {
                continue;
            }
        }

        // Remove foreign languages
        $category = $item->category;
        if (strpos($category,'Movies-Foreign') !== false) {
            continue;
        }

        $guid = $item->guid;
        $namespaces = $item->getNameSpaces(true);
        $newznab = $item->children($namespaces['newznab']);
        $grabs = "";
        foreach ($newznab as $i) {
            if ($i->attributes()->name == "grabs") {
                $grabs = $i->attributes()->value;
            }
        }

        $array = array (
            "title" => $title,
            "grabs" => $grabs,
            "guid" => $guid,
        );
        array_push($nzbArray, $array);
    }

    // Convert XML Object into an Array via JSON
    $json = json_encode($nzbArray);
    $nzbArray = json_decode($json,TRUE);

    if (count($nzbArray) >= 1) {
        usort($nzbArray, 'cmp');
    }

    $results = count($nzbArray);
    if ($results == 0) {
        logit($con, "INFO", "No results found", $movie['name'], "tt".$reqimdbID, NULL, $searchProvider['providerName']);
        return false;
    }

    $foundTitle = $nzbArray[0]['title'][0];
    $foundGrabs = $nzbArray[0]['grabs'][0];
    $foundGUID = $nzbArray[0]['guid'][0];

    $url = $url."api?apikey=".$api."&t=cartadd&id=".$foundGUID;
    $xmlString = file_get_contents($url);
    if ($xmlString === false) {
        logit($con, "ERR", "Failed to add movie to cart...", $movie['name'], "tt".$reqimdbID, $foundGUID, $searchProvider['providerName']);
        return false;
    }
    logit($con, "INFO", "Movie Found ($foundGrabs grabs) - Adding to cart...", $movie['name'], "tt".$reqimdbID, $foundGUID, $searchProvider['providerName']);

    return true;
}


// function to sort an array in descending order based on the number of grabs
function cmp($a, $b) {
    if ($a['grabs'] == $b['grabs']) {
        return 0;
    }
    return ($a['grabs'] < $b['grabs']) ? 1 : -1;
}


// function to log messages to the DB
function logit($con, $type, $message, $nzbName=NULL, $imdbID=NULL, $nzbGUID=NULL, $searchProvider=NULL) {
    if ($type == "ERR") {
        $type = 1;
    }
    elseif ($type == "INFO") {
        $type = 2;
    }
    elseif ($type == "DL") {
        $type = 3;
    }
    else {
        $type = 4;
    }

    if($searchProvider!=NULL) {
        $sql = sprintf(
            "INSERT INTO log (searchProviderID, timestamp, type, message, nzbName, imdbID, nzbGUID)
                SELECT id, NOW(), '%d', '%s', '%s', '%s', '%s' FROM searchProviders WHERE providerName = '$searchProvider'",
            $con->real_escape_string($type),
            $con->real_escape_string($message),
            $con->real_escape_string($nzbName),
            $con->real_escape_string($imdbID),
            $con->real_escape_string($nzbGUID)
        );
    }
    else {
        $sql = sprintf(
            "INSERT INTO log (timestamp, type, message, nzbName, imdbID, nzbGUID, searchProviderID)
            VALUES (NOW(), '%d', '%s', NULL, NULL, NULL, NULL)",
            $con->real_escape_string($type),
            $con->real_escape_string($message)
        );
    }
    if ( ! $con->query($sql) ) {
        printf("%s\n", $con->error);
        die;
    }
}

?>
