<?php
include_once('includes/php/db.inc.php');
include_once('includes/php/config.php');
include_once('includes/php/functions.php');

// Authentication System stolen from php.net
$realm = 'Restricted Area';

if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
    header('HTTP/1.1 401 Unauthorized');
    header('WWW-Authenticate: Digest realm="'.$realm.
           '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

    die('You chose, poorly');
}

// analyze the PHP_AUTH_DIGEST variable
if (!($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
    !isset($users[$data['username']]))
    die('Wrong Credentials!');


// generate the valid response
$A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

if ($data['response'] != $valid_response)
    die('Wrong Credentials!');

// ok, valid username & password

$yearNow = date("Y");

// Ensure we have data passed to this page
if (isset($_GET['id']) && !empty($_GET['id']) ) {
    $id = $_GET['id'];
}
else {
    echo "bad data entered id:'".$_GET['id']."'... exiting";
    die;
}

?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Movies</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="includes/js/pace.js"></script>
        <link href="includes/css/pace.css" rel="stylesheet" />
        <link href="includes/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    </head>
    <body>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="/movies" class="navbar-brand">Rad Mullet</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li>
              <a href="about.php">About</a>
            </li>
            <li>
              <a href="donate.php">Donate</a>
            </li>
            <li>
              <a href="logs.php">Logs</a>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Suggesting Viewing <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <?php
                    $i = 1;
                    while ($i <= 3) {
                        echo '<li><a tabindex="-1" href="http://www.imdb.com/year/'.$yearNow.'/" target="_blank">Popular Feature Films of '.$yearNow.'</a></li>';
                        $yearNow--;
                        $i++;
                    }
                ?>
                <li><a tabindex="-1" href="http://www.imdb.com/chart/top" target="_blank">Top 250</a></li>
                <li><a tabindex="-1" href="http://www.imdb.com/chart/bottom" target="_blank">Bottom 100</a></li>
              </ul>
            </li>

          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Settings <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <li><a tabindex="-1" href="../default/">Logout</a></li>
                <li class="divider"></li>
                <li><a tabindex="-1" href="../amelia/">General</a></li>
                <li><a tabindex="-1" href="../cerulean/">Search Settings</a></li>
                <li><a tabindex="-1" href="../cosmo/">Search Providers</a></li>
                <li><a tabindex="-1" href="../cyborg/">Post Processing</a></li>
                <li><a tabindex="-1" href="../flatly/">Notifications</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-6">
                    <h1>Rad Mullet</h1>
                    <p class="lead">Movie Details...</p>
                </div> <!-- col-lg-6 -->

                <div class="col-lg-3">
                    <div class="bsa well">
                        <img src="includes/img/logo.gif" alt="logo" class="img-rounded">
                    </div> <!-- bsa well -->
                </div> <!-- col-lg-6 -->
                <div class="col-lg-3">
                </div>
            </div> <!-- row -->
        </div> <!-- page-header -->

        <?php
            # Grab the movieDetails from the DB
            $sql = "SELECT name,
                    year,
                    IF(quality = 1,'1080p',
                        IF(quality = 2,'720p',
                            IF(quality = 3,'SD','Any')
                        )
                    ) AS quality, 
                    status, 
                    dateAdded, 
                    lastSearched, 
                    imdbID, 
                    imdbPoster 
                    FROM movieData 
                    WHERE id = $id";

            $result = mysqli_query($con, $sql);
            if ( ! $con->query($sql) ) {
                printf("%s\n", $con->error);
                die;
            }

            while ($row = mysqli_fetch_assoc($result)) {
                $name = $row['name'];
                $year = $row['year'];
                $quality = $row['quality'];
                $status = $row['status'];
                $dateAdded = $row['dateAdded'];
                $lastSearched = $row['lastSearched'];
                $imdbID = $row['imdbID'];
                $imdbPoster = $row['imdbPoster'];
            }
        ?>

        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="well well-sm">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <img src="includes/php/image.php?image_src=<?php print $imdbPoster;?>" alt="IMDB Poster" class="img-rounded img-responsive" />
                            </div>
                            <div class="col-sm-6 col-md-8">
                                <h4><?php print $name;?></h4>
                                <p>
                                    Year: <?php print $year;?>
                                    <br />
                                    Quality: <?php print $quality;?>
                                    <br />
                                    Status: <?php print $status;?>
                                    <br />
                                    Added: <?php print $dateAdded;?>
                                    <br />
                                    Last Searched: <?php if ($lastSearched = "0000-00-00 00:00:00") {
                                        print "Never!";
                                    }
                                    else print $lastSearched;?>
                                    <br />
                                    <a href="http://imdb.com/title/<?php print $imdbID;?>" target="_blank"> IMBD Link</a>
                                    <br />
                                    <a href="movieDetails.php?id=<?php print $id; ?>&edit=true">Edit Movie</a>
                                    <br />
                                    <a href="movieDetails.php?id=<?php print $id; ?>&delete=true">Delete Movie</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="http://code.jquery.com/jquery.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="includes/js/bootstrap.min.js"></script>

    </body>
</html>

<?php
mysqli_close($con);
?>

