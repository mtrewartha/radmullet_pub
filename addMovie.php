<?php
include_once('includes/php/db.inc.php');
include_once('includes/php/config.php');
include_once('includes/php/functions.php');

// Authentication System stolen from php.net
$realm = 'Restricted Area';

if (empty($_SERVER['PHP_AUTH_DIGEST'])) {
    header('HTTP/1.1 401 Unauthorized');
    header('WWW-Authenticate: Digest realm="'.$realm.
           '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');

    die('You chose, poorly');
}

// analyze the PHP_AUTH_DIGEST variable
if (!($data = http_digest_parse($_SERVER['PHP_AUTH_DIGEST'])) ||
    !isset($users[$data['username']]))
    die('Wrong Credentials!');


// generate the valid response
$A1 = md5($data['username'] . ':' . $realm . ':' . $users[$data['username']]);
$A2 = md5($_SERVER['REQUEST_METHOD'].':'.$data['uri']);
$valid_response = md5($A1.':'.$data['nonce'].':'.$data['nc'].':'.$data['cnonce'].':'.$data['qop'].':'.$A2);

if ($data['response'] != $valid_response)
    die('Wrong Credentials!');

// ok, valid username & password

$yearNow = date("Y");



// Ensure we have data passed to this page
if (isset($_GET['imdbID']) && !empty($_GET['imdbID']) ) {
    $imdbID = $_GET['imdbID'];
}
else {
echo "bad data entered imdbID:'".$_GET['imdbID']."'... exiting";
    die;
}

// Get the actual details of this film from the omdbapi.com
$movieDetails = getMovieDetails($imdbID);

$title = $movieDetails[0];
$year = $movieDetails[1];
//$imdbID = $movieDetails[2];
$poster = $movieDetails[3];

##### GET THIS FROM DB #####
$quality = '2';

# Insert movie to DB:
$sql = sprintf(
    "INSERT INTO movieData (name, year, quality, dateAdded, imdbID, imdbPoster) VALUES ('%s', '%s', '%d', NOW(), '%s', '%s')",
    $con->real_escape_string($title),
    $con->real_escape_string($year),
    $con->real_escape_string($quality),
    $con->real_escape_string($imdbID),
    $con->real_escape_string($poster)
);
if ( ! $con->query($sql) ) {
    printf("%s\n", $con->error);
    die;
}


?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Movies</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="refresh" content="2;URL='index.php'" />
        <script src="includes/js/pace.js"></script>
        <link href="includes/css/pace.css" rel="stylesheet" />
        <link href="includes/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    </head>
    <body>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="/movies" class="navbar-brand">Rad Mullet</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li>
              <a href="about.php">About</a>
            </li>
            <li>
              <a href="donate.php">Donate</a>
            </li>
            <li>
              <a href="logs.php">Logs</a>
            </li>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Suggesting Viewing <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <?php
                    $i = 1;
                    while ($i <= 3) {
                        echo '<li><a tabindex="-1" href="http://www.imdb.com/year/'.$yearNow.'/" target="_blank">Popular Feature Films of '.$yearNow.'</a></li>';
                        $yearNow--;
                        $i++;
                    }
                ?>
                <li><a tabindex="-1" href="http://www.imdb.com/chart/top" target="_blank">Top 250</a></li>
                <li><a tabindex="-1" href="http://www.imdb.com/chart/bottom" target="_blank">Bottom 100</a></li>
              </ul>
            </li>

          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Settings <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <li><a tabindex="-1" href="../default/">Logout</a></li>
                <li class="divider"></li>
                <li><a tabindex="-1" href="../amelia/">General</a></li>
                <li><a tabindex="-1" href="../cerulean/">Search Settings</a></li>
                <li><a tabindex="-1" href="../cosmo/">Search Providers</a></li>
                <li><a tabindex="-1" href="../cyborg/">Post Processing</a></li>
                <li><a tabindex="-1" href="../flatly/">Notifications</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-6">
                    <h1>Rad Mullet</h1>
                    <p class="lead">Adding movie...</p>
                </div> <!-- col-lg-6 -->

                <div class="col-lg-3">
                    <div class="bsa well">
                        <img src="includes/img/logo.gif" alt="logo" class="img-rounded">
                    </div> <!-- bsa well -->
                </div> <!-- col-lg-6 -->
                <div class="col-lg-3">
                </div>
            </div> <!-- row -->
        </div> <!-- page-header -->

        <div class="row">
            <table class="table table-striped">
                <thead>
                    <th>Title</th>
                    <th>Year</th>
                    <th>Quality</th>
                    <th>Status</th>
                    <th>IMDB ID</th>
                    <th>IMDB Poser</th>
                </thead>
                <tbody id="searchResults">
                    <td><?php echo "$title";?></td>
                    <td><?php echo "$year";?></td>
                    <td><?php echo "$quality";?></td>
                    <td>Just Added...</td>
                    <td><?php echo "$imdbID";?></td>
                    <td><img src="includes/php/image.php?image_src=<?php echo "$poster";?>" alt="imdb poster"</td>
                </tbody>
            </table>
        </div> <!-- row -->

    </div> <!-- container -->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="http://code.jquery.com/jquery.js"></script> -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="includes/js/bootstrap.min.js"></script>

    </body>
</html>

<?php
mysqli_close($con);
?>
